package com.example.demo.repository;

import com.example.demo.model.Book;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String addBook(@Param("book") Book book){
        return new SQL(){{
            INSERT_INTO("tb_books");
            VALUES("title, author, description, thumbnail, category_id","#{book.title},#{book.author},#{book.description},#{book.thumbnail},#{book.categoryID}");
        }}.toString();
    }

    public String findBook(){
        return new SQL(){{
            SELECT("b.*,c.*");
            FROM("tb_books b");
            INNER_JOIN("tb_categories c on b.category_id=c.id");
        }}.toString();
    }

    public String updateBook(@Param("book") Book book, int id){
        return new SQL(){{
            UPDATE("tb_books");
            SET("title = #{book.title}, author = #{book.author}, description = #{book.description}, thumbnail = #{book.thumbnail}, category_id = #{book.categoryID}");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String deleteBook(int id){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String resetBookCat(){
        return  new SQL(){{
            UPDATE("tb_books");
            SET("category_id = null}");
        }}.toString();
    }
}
