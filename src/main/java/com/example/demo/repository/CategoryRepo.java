package com.example.demo.repository;

import org.apache.ibatis.annotations.InsertProvider;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepo {

    @InsertProvider(type = CategoryProvider.class, method = "deleteCategory")
    boolean deleteCategory(int id);
}
