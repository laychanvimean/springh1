package com.example.demo.repository;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String deleteCategory(int id){
        return new SQL(){{
            DELETE_FROM("tb_categories");
            WHERE("id=#{id}");

        }}.toString();
    }
}
