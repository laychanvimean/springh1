package com.example.demo.repository;

import com.example.demo.model.Book;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepo {

    @InsertProvider(type = BookProvider.class, method = "addBook")
    boolean addBook(@Param("book") Book book);

    @SelectProvider(type = BookProvider.class, method = "findBook")
    @Results({
            @Result(column = "category.id", property = "category_id"),
            @Result(column = "category.name", property = "title")
    })
    List<Book> findBook();

    @Select("SELECT b.*,c.title AS \"cat_title\" FROM tb_books b INNER JOIN tb_categories c ON b.category_id = c.id")
    @Results({
            @Result(column = "title", property = "title"),
            @Result(column = "category.id", property = "category_id"),
            @Result(column = "cat_title", property = "category.name")
    })
    List<Book> showBook();

    @InsertProvider(type = BookProvider.class, method = "updateBook")
    boolean updateBook(@Param("book") Book book, int id);

    @InsertProvider(type = BookProvider.class, method = "deleteBook")
    boolean deleteBook(int id);

    @InsertProvider(type = BookProvider.class, method = "resetBookCat")
    boolean resetBookCat();
}
