package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.service.BookService;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/book")
public class BookController {

    BookService bookService;
    CategoryService categoryService;

    @Autowired
    public BookController(BookService bookService,CategoryService categoryService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
    }


    @GetMapping("/findall")
    public Map<String, Object> findBook(){
        Map<String, Object> result = new HashMap<>();
        List<Book> book = bookService.findBook();
        result.put("book", book);

        return result;
    }

    @PostMapping("/addBook")
    public Map<String, Object> addBook(@RequestBody Book book){
        Map<String, Object> result = new HashMap<>();
        if(bookService.addBook(book)){
            result.put("success", "true");
        }else {
            result.put("success", "Fail");
        }
        return result;
    }

    @GetMapping("/showbook")
    public Map<String, Object> showBook(){
        Map<String, Object> result = new HashMap<>();
        List<Book> books = bookService.showBook();;
        result.put("code", "200");
        result.put("success", true);
        result.put("products", books);

        return result;

    }

    @PutMapping("/{id}")
    public Map<String, Object> updateBook(@RequestBody Book book, @PathVariable int id){
        Map<String, Object> result = new HashMap<>();
        if(bookService.updateBook(book, id)){
            result.put("success", "true");
        }else {
            result.put("success", "Fail");
        }
        return result;
    }

    @DeleteMapping("/{id}")
    public Map<String, Object> deleteBook( @PathVariable int id){
        Map<String, Object> result = new HashMap<>();
        if(bookService.deleteBook(id)){
            result.put("success", "true");
        }else {
            result.put("success", "Fail");
        }
        return result;
    }

    @DeleteMapping("/cat/{id}")
    public Map<String, Object> deleteCategory( @PathVariable int id){
        Map<String, Object> result = new HashMap<>();
        if(categoryService.deleteCategory(id)){
            result.put("success", "true");
            bookService.resetBookCat();
        }else {
            result.put("success", "Fail");
        }
        return result;
    }

}
