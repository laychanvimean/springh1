package com.example.demo.service;

import com.example.demo.model.Book;
import com.example.demo.repository.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    BookRepo bookRepo;

    @Autowired
    public BookServiceImp(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }

    @Override
    public List<Book> findBook() {
        return bookRepo.findBook();
    }

    @Override
    public List<Book> showBook() {
        return bookRepo.showBook();
    }

    @Override
    public boolean addBook(Book book) {
        return bookRepo.addBook(book);
    }

    @Override
    public boolean updateBook(Book book, int id) {
        return bookRepo.updateBook(book, id);
    }

    @Override
    public boolean deleteBook(int id) {
        return bookRepo.deleteBook(id);
    }

    @Override
    public boolean resetBookCat() {
        return bookRepo.resetBookCat();
    }
}
