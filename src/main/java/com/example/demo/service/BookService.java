package com.example.demo.service;

import com.example.demo.model.Book;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {

    List<Book> findBook();

    List<Book> showBook();

    boolean addBook(@Param("book") Book book);

    boolean updateBook(@Param("book") Book book, int id);

    boolean deleteBook(int id);

    boolean resetBookCat();
}
