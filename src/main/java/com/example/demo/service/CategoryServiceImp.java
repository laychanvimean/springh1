package com.example.demo.service;

import com.example.demo.repository.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImp implements CategoryService{

    CategoryRepo categoryRepo;

    @Autowired
    public CategoryServiceImp(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @Override
    public boolean deleteCategory(int id) {
        return categoryRepo.deleteCategory(id);
    }
}
